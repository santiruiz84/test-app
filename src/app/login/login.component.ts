import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  private isValidEmail = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'
  passMinLength = 5;
  hide = true;
  loading = false;
  submitted = false;
  form = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
    reminder: new FormControl(false),
  });

  constructor(private formBuilder: FormBuilder) {  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.isValidEmail)]],
      password: ['', [Validators.required, Validators.minLength(this.passMinLength)]],
      reminder: [false],
    });
  }

  onSubmit() {
    if(!this.form.invalid) {
      this.loading = true;
      this.submitted = true;
      console.log('OK');
      console.log('email: ', this.form.value.email);
      console.log('password: ', this.form.value.password);
      console.log('reminder: ', this.form.value.reminder);
    }
  }

  isValidField(field: string) {
    if (!this.form.get(field)) { return false; }
    const formField = this.form.get(field);

    return (formField?.touched || formField?.dirty || this.getErrorMessage(field)) && 
           !formField?.valid;
  }

  getErrorMessage(field: string): string | undefined {
    let message;
    const formField = this.form.get(field);

    if (formField?.errors?.['required']) {
      message = 'Campo requerido';
    } else if (formField?.hasError('pattern')) {
      message = 'No es un email correcto';
    } else if (formField?.hasError('minlength')) {
      message = `La contraseña tiene que tener como mínimo ${this.passMinLength} carácteres`;
    }

    return message;
  }

}
