import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { blankLogin, invalidEmailInvalidLengthPassword, validLogin } from 'src/mocks/users';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ 
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatInputModule,
        MatProgressBarModule,
        MatIconModule,
        MatButtonModule,
        MatSlideToggleModule, 
      ],
      declarations: [ LoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  function setFormValues(email: string, password: string, reminder: boolean) {
    component.form.controls['email'].setValue(email);
    component.form.controls['password'].setValue(password);
    component.form.controls['reminder'].setValue(reminder);
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('component initial state', () => {
    expect(component.submitted).toBeFalsy();
    expect(component.form).toBeDefined();
    expect(component.form.invalid).toBeTruthy();
  });

  it('form value should update when change the input', (() => {
    setFormValues(validLogin.email, validLogin.password, validLogin.reminder);
    expect(component.form.value).toEqual(validLogin);
  }));

  it('form invalid should be true when form is invalid', (() => {
    setFormValues(blankLogin.email, blankLogin.password, blankLogin.reminder);
    expect(component.form.invalid).toBeTruthy();
  }));

  it('display email error msg when email is blank', () => {
    setFormValues(blankLogin.email, validLogin.password, blankLogin.reminder);
    fixture.detectChanges();

    const button = fixture.debugElement.nativeElement.querySelector('#login-button');
    button.click();
    fixture.detectChanges();

    const emailErrorMsg = fixture.debugElement.nativeElement.querySelector('#email-error-msg');
    expect(emailErrorMsg).toBeDefined();
    expect(emailErrorMsg.innerHTML).toContain('Campo requerido');
  });

  it('display email error msg when email is invalid', () => {
    setFormValues(invalidEmailInvalidLengthPassword.email, validLogin.password, blankLogin.reminder);
    fixture.detectChanges();

    const button = fixture.debugElement.nativeElement.querySelector('#login-button');
    button.click();
    fixture.detectChanges();

    const emailErrorMsg = fixture.debugElement.nativeElement.querySelector('#email-error-msg');
    expect(emailErrorMsg).toBeDefined();
    expect(emailErrorMsg.innerHTML).toContain('No es un email correcto');
  });

  it('display password error msg when password is blank', () => {
    setFormValues(validLogin.email, blankLogin.password, blankLogin.reminder);
    fixture.detectChanges();

    const button = fixture.debugElement.nativeElement.querySelector('#login-button');
    button.click();
    fixture.detectChanges();

    const passwordErrorMsg = fixture.debugElement.nativeElement.querySelector('#password-error-msg');
    expect(passwordErrorMsg).toBeDefined();
    expect(passwordErrorMsg.innerHTML).toContain('Campo requerido');
  });

  it('display password error msg when password length is invalid', () => {

    const passwordLengthErrorMsg = `La contraseña tiene que tener como mínimo ${component.passMinLength} carácteres`;

    setFormValues(validLogin.email, invalidEmailInvalidLengthPassword.password, blankLogin.reminder);
    fixture.detectChanges();

    const button = fixture.debugElement.nativeElement.querySelector('#login-button');
    button.click();
    fixture.detectChanges();

    const passwordErrorMsg = fixture.debugElement.nativeElement.querySelector('#password-error-msg');
    expect(passwordErrorMsg).toBeDefined();
    expect(passwordErrorMsg.innerHTML).toContain(passwordLengthErrorMsg);
  });

  it('display email and password error msg when email and password is blank', () => {
    setFormValues(blankLogin.email, blankLogin.password, blankLogin.reminder);
    fixture.detectChanges();

    const button = fixture.debugElement.nativeElement.querySelector('#login-button');
    button.click();
    fixture.detectChanges();

    const emailErrorMsg = fixture.debugElement.nativeElement.querySelector('#email-error-msg');
    expect(emailErrorMsg).toBeDefined();
    expect(emailErrorMsg.innerHTML).toContain('Campo requerido');

    const passwordErrorMsg = fixture.debugElement.nativeElement.querySelector('#password-error-msg');
    expect(passwordErrorMsg).toBeDefined();
    expect(passwordErrorMsg.innerHTML).toContain('Campo requerido');
  });

  it('display email error msg when email is blank', () => {
    setFormValues(blankLogin.email, validLogin.password, blankLogin.reminder);
    fixture.detectChanges();

    const button = fixture.debugElement.nativeElement.querySelector('#login-button');
    button.click();
    fixture.detectChanges();

    const emailErrorMsg = fixture.debugElement.nativeElement.querySelector('#email-error-msg');
    expect(emailErrorMsg).toBeDefined();
    expect(emailErrorMsg.innerHTML).toContain('Campo requerido');
  });

  it('when email is blank, email field should display invalid mode', () => {
    setFormValues(blankLogin.email, validLogin.password, validLogin.reminder);
    fixture.detectChanges();
    
    const button = fixture.debugElement.nativeElement.querySelector('#login-button');
    button.click();
    fixture.detectChanges();

    const emailInput = fixture.debugElement.nativeElement.querySelector('#email-input');
    expect(emailInput.classList).toContain('ng-invalid');
  });

  it('when password is blank, password field should display invalid mode', () => {
    setFormValues(validLogin.email, blankLogin.password, validLogin.reminder);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('#login-button');
    button.click();
    fixture.detectChanges();

    const passwordInput = fixture.debugElement.nativeElement.querySelector('#password-input');
    expect(passwordInput.classList).toContain('ng-invalid');
  });

  it('display loading when form valid submit', () => {
    setFormValues(validLogin.email, validLogin.password, validLogin.reminder);
    fixture.detectChanges();

    const button = fixture.debugElement.nativeElement.querySelector('#login-button');
    button.click();
    fixture.detectChanges();

    expect(component.submitted).toEqual(true);
  });

  it('no display loading when form invalid submit', () => {
    setFormValues(invalidEmailInvalidLengthPassword.email, validLogin.password, validLogin.reminder);
    fixture.detectChanges();

    const button = fixture.debugElement.nativeElement.querySelector('#login-button');
    button.click();
    fixture.detectChanges();
    
    expect(component.submitted).toEqual(false);
  });

  it('validate isValidField return when form field not exist', () => {
    expect(component.isValidField('field-unknown')).toEqual(false);
  });

  it('validate getErrorMessage return when form field not exist', () => {
    expect(component.getErrorMessage('field-unknown')).toEqual(undefined);
  });
});
