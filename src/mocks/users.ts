export const blankLogin = {
    email: '',
    password: '',
    reminder: false
};

export const validLogin = {
    email: 'correo@prueba.com',
    password: '12345',
    reminder: false
};

export const validLoginReminder = {
    email: 'correo@prueba.com',
    password: '12345',
    reminder: true
};


export const invalidEmailInvalidLengthPassword = {
    email: 'prueba.com',
    password: '1234',
    reminder: false
};